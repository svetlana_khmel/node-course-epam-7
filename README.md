# HOMEWORK 7 #

### NoSQL DATABASES. ODM ###

### Tasks ###

``````````

Install mongodb (any way acceptable but the usage of docker container is preferred).
Create database in mongodb.
Create collection cities and fill it with the mock data by adding multiple documents into it with the following schema (or a similar one):

{
    name: ‘Brest’,
    country: ‘Belarus’,
    capital: false,
    location: {
        lat: 52.097621,
        long: 23.734050
    }
}

Write a simple web server which will return a random city on every request to it (you can modify already existed one).
Install mongoose package.
Make your solution for task 4 use mongoose instead of the native implementation (define city model).
Create models for user and product via mongoose (use appropriate module files from Homework 1).
Generate mock data for users and products and import all of them via mongoose in users and products collections inside the database.
Add validations for appropriate fields of your models (e.g. capital field in city model).
Modify application to respond all routes from Homework 4 and return data from the database.

Add additional routes and make your application responds on them:

URL
METHOD
ACTION
/api/users/:id
DELETE
Deletes SINGLE user
/api/products/:id
DELETE
Deletes SINGLE product
/api/cities
GET
Returns ALL cities
/api/cities
POST
Adds NEW city and returns it
/api/cities/:id
PUT
Updates SINGLE city by id if exists or adds NEW city with the given id otherwise
/api/cities/:id
DELETE
Deletes SINGLE city

Implement a function which will add extra field called lastModifiedDate with the current date for every created/updated item (every PUT and POST request for all user, product and city entities).

Evaluation Criteria
All required packages installed, database created (tasks 1-2).
Simple web server implemented which returns random city in response (tasks 3-5).
All models implemented and data imported to database (task 6-8).
Validations applied and all implemented routes return data from database (tasks 9-10).
All routes (including additional) return data from database with extra field added automatically on creation/update (task 11-12).




``````````


** Accidentally committed .idea directory files into git **
$ echo '.idea' >> .gitignore
$ git rm -r --cached .idea
$ git add .gitignore
$ git commit -m '(some message stating you added .idea to ignored entries)'
$ git push


##   Random record from MongoDB ##

Do a count of all records, generate a random number between 0 and the count, and then do:

db.yourCollection.find().limit(-1).skip(yourRandomNumber).next()

show collections

https://www.tutorialspoint.com/mongodb/mongodb_environment.htm

https://www.mongodb.com/blog/post/introducing-version-40-mongoose-nodejs-odm

https://medium.freecodecamp.org/introduction-to-mongoose-for-mongodb-d2a7aa593c57

https://codeburst.io/using-mongoose-validation-with-async-await-c3a9255459e1

https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/mongoose



## Swagger ##

https://swagger.io/resources/webinars/design-apis-with-openapi-specification/

https://swagger.io/docs/specification/basic-structure/

https://learn.getgrav.org/advanced/change-site-url
https://swagger.io/docs/specification/2-0/basic-structure/


## Swagger:Issue with Path parameter ##

Basically, you're declaring a path that has a path parameter in it, by using path templates. In this case {id} declares a path parameter called id.

When you declare such a path, it means that you have to declare that path parameter as part of the operation.

Take a look at this YAML example:

  /pets/{id}:
    get:
      description: Returns a user based on a single ID, if the user does not have access to the pet
      operationId: findPetById
      produces:
        - application/json
        - application/xml
        - text/xml
        - text/html
      parameters:
        - name: id
          in: path
          description: ID of pet to fetch
          required: true
          type: integer
          format: int64
      responses:
        '200':
          description: pet response
          schema:
            $ref: '#/definitions/pet'
        default:
          description: unexpected error
          schema:
            $ref: '#/definitions/errorModel'
You can see there's an {id} in the path, and a corresponding id parameter definition. Without it, the spec won't be valid.



## Duplicate mapping key ##

Put them both under the same path and it will work. JSON doesn't allow duplicate keys.

https://www.npmjs.com/package/swagger

https://swagger.io/docs/specification/media-types/
https://swagger.io/docs/specification/2-0/describing-parameters/#form-parameters
https://swagger.io/docs/specification/describing-responses/
https://swagger.io/docs/specification/2-0/describing-request-body/
https://swagger.io/docs/specification/2-0/paths-and-operations/
https://swagger.io/docs/specification/2-0/paths-and-operations/



