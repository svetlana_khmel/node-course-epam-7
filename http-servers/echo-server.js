const server = require('http').createServer();
const { Readable } = require('stream');

server.on('request', (req, res) => {
  let data = [];

  req
    .on('data', (chunk) => {
      data.push(chunk);
    })
    .on('end', () => {
      const inStream = new Readable({
        read() {}
      });

      inStream.push(data.toString());

      if (data.toString().length === 0) {
        inStream.push('No data');
      }

      inStream.push(null); // No more data
      inStream.pipe(res);
    });
});

server.listen(3000);
