const users_data = require('../data/users.json');

const getUsers = () => {
    return users_data;
};

module.exports = getUsers;