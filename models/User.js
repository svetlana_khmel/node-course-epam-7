const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: true},
    lastModifiedDate: Number
});

module.exports = mongoose.model('User', userSchema);