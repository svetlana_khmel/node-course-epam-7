//"options":[{"color":"blue"},{"size":"XL"}]

const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const productSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  brand: String,
  price: Number,
  options: Schema.Types.Mixed,
  review: String,
  lastModifiedDate: Number
});

module.exports = mongoose.model('Product', productSchema);