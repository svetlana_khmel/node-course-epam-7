const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;
let validate = require('mongoose-validator');

let countryValidator = [
  validate({
    validator: 'matches',
    arguments: /^[A-Z]/g,
    //message: '{VALUE} name should start from capital letter!',
  })
]

const citySchema = new Schema({
  name: {
    type: String,
    validate: [nameValidator, '{VALUE} name should start from capital letter!'],
  },
  country: {
    type: String,
    validate: countryValidator
  },
  capital: Boolean,
  location: {
    lat: Number,
    long: Number
  },
  lastModifiedDate: Number
});

module.exports = mongoose.model('City', citySchema);

function nameValidator (value) {
  return /^[A-Z]/.test(value);
}

citySchema.pre('save', function(next) {
  this.options.runValidators = true;
  next();
});
