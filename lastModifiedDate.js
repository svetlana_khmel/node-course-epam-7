module.exports = (req, res, next) => {
  //lastModifiedDate
  req.lastModifiedDate = new Date();
  next();
}