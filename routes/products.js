const express = require('express');
const router = express.Router();
const uuidv1 = require('uuid/v1');
const getProducts = require('../controllers/products');
const parsedCookies = require('../middlewares/parsedCookies');
const parsedQuery = require('../middlewares/parsedQuery');
const Product = require('../models/Product');

router.use(parsedCookies);
router.use(parsedQuery);


router.get('/', (req, res) => {
  Product.find({}, (err, products) => {
    if (err) {
      return next(err);
    }
    res.json(users);
  });
});

router.get('/:id', (req, res) => {
  Product.findOne({_id: req.params.id}, function (err, product) {
    if (err) {
      return next(err);
    }
    res.json(product);
  });
});

router.get('/:id/reviews', (req, res) => {
  Product.findOne({_id: req.params.id}, function (err, product) {
    if (err) {
      return next(err);
    }
    res.json(product[0].review);
  });
});

// Send POST request with data:
// {
// "name": "Supreme T-Shirt 10",
//   "brand": "Supreme",
//   "price": "99.99",
//   "options": [
//   { "color": "blue" },
//   { "size": "XL" }
// ],
//   "review": "IMDb goes behind-the-scenes with movie prop master."
// }

router.post('/', (req, res) => {
  const data = Object.assign({},{lastModifiedDate: req.lastModifiedDate}, req.body);

  let product = new Product(
    data
  );

  product.save(function(err, product) {
    res.json(product);
  });
});

router.delete('/:id', (req, res, next) => {
  Product.findOneAndDelete({'_id':req.params.id}, (err, product) => {
    if (err) return next(err);
    res.json(product);
  })
});

module.exports = router;

