const express = require('express');
const router = express.Router();
const User = require('../models/User');

router.get('/', (req, res) => {
  User.find({}, (err, users) => {
    if (err) {
      return next(err);
    }
    res.json(users);
  })
});

router.delete('/:id', (req, res, next) => {
  User.findOneAndDelete({'_id':req.params.id}, (err, user) => {
    if (err) return next(err);
    res.json(user);
  })
});

module.exports = router;
