const express = require('express');
let router = express.Router();
const City = require('../models/City');

router.get('/random', (req, res, next) => {
  City.countDocuments().exec((err, count) => {

    let random = Math.floor(Math.random() * count)

    City.findOne().skip(random).exec(
      (err, result) => {
        console.log(result)
        res.json(result);
      })
  })
});

router.get('/', (req, res, next) => {
  City.find({}, (err, orders) => {
    if (err) {
      return next(err);
    }
    res.json(orders);
  })
});

router.post('/', (req, res, next) => {
  const data = Object.assign({},{lastModifiedDate: req.lastModifiedDate}, req.body);

  let city = new City(
    data
  );

  city.save((err, city) => {
    res.json(city);
  });
});

router.put('/:id', (req, res, next) => {
  const data = Object.assign({},{lastModifiedDate: req.lastModifiedDate}, req.body);

  City.update({'_id': req.params.id}, data, {upsert: true, setDefaultsOnInsert: true}, (err, city) => {
    res.json(city);
  });
})

router.delete('/:id', (req, res, next) => {
  City.findOneAndDelete({'_id':req.params.id}, (err, city) => {
    if (err) return next(err);
    res.json(city);
  })
});

module.exports = router;
