const express = require('express');
const app = express();
const mongoose = require('mongoose');
const config = require('./config');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const products = require('./routes/products');
const user = require('./routes/users');
const cities = require('./routes/cities');
const lastModifiedDate = require('./lastModifiedDate');

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(lastModifiedDate);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Task 1 is commented.
// const MongoClient = require('mongodb').MongoClient;
// const assert = require('assert');
//
// // Connection URL
// const url = 'mongodb://localhost:27017';
//
// // Database Name
// const dbName = 'cities';
//
// const items = [
//   {name: 'Brest1', country: 'Belarus1', capital: false,location: {lat: 52.097621,long: 23.734050}},
//   {name: 'Brest2', country: 'Belarus2', capital: false,location: {lat: 52.097621,long: 23.734050}},
//   {name: 'Brest3', country: 'Belarus3', capital: false,location: {lat: 52.097621,long: 23.734050}},
//   {name: 'Brest4', country: 'Belarus4', capital: false,location: {lat: 52.097621,long: 23.734050}},
//   {name: 'Brest5', country: 'Belarus5', capital: false,location: {lat: 52.097621,long: 23.734050}}
// ]
//
// // Task 4
//  MongoClient.connect(url, function(err, client) {
//    assert.equal(null, err);
//    console.log("Connected correctly to server");
//
//    const db = client.db(dbName);
//
//    // Insert multiple documents
//    db.collection('cities').insertMany(items, function(err, r) {
//      console.log("inserted", r)
//      client.close();
//    });
//
//    let count = null;
//
//    db.collection('cities').find(items, () => {
//      count = items.length;
//    });
//
//    db.collection('cities').find().limit(-1).skip(Math.floor(Math.random()*count)).toArray(function(err, docs) {
//      console.log("docs    ", docs)
//
//      client.close();
//    });
//  });
//

const dbUrl = config.database;

console.log("DB URL.... ", dbUrl);
mongoose.connect(dbUrl).then(
  () => {
    console.log(`MongoDB is connected.`);
  },
  err => {
    console.log(`Mongoose Connection ERROR: ${err}`);
  }
);

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.use('/api/products', products);
app.use('/api/users', user);
app.use('/api/cities', cities);

export default app;










